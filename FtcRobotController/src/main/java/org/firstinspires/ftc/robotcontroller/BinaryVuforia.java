import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.hardware.DcMotor
import java.util.List
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaSkyStone
import org.firstinspires.ftc.robotcore.external.tfod.Recognition
import org.firstinspires.ftc.robotcore.external.tfod.TfodSkyStone

    @TeleOp(name = "BinaryTensorflow (Blocks to Java)", group = "")
    public class BinaryTensorflow extends LinearOpMode {

        private VuforiaSkyStone vuforiaSkyStone;
        private TfodSkyStone tfodSkyStone;
        private DcMotor left;
        private DcMotor right;

        /**
         * This function is executed when this Op Mode is selected from the Driver Station.
         */
        @Override
        public void runOpMode() {
            List<Recognition> recognitions;

            vuforiaSkyStone = new VuforiaSkyStone();
            tfodSkyStone = new TfodSkyStone();
            left = hardwareMap.get(DcMotor.class, "left");
            right = hardwareMap.get(DcMotor.class, "right");

            // Sample TFOD Op Mode
            // Initialize Vuforia.
            // This sample assumes phone is in landscape mode.
            // Rotate phone -90 so back camera faces "forward" direction on robot.
            // We need Vuforia to provide TFOD with camera images.
            vuforiaSkyStone.initialize(
                    "", // vuforiaLicenseKey
                    VuforiaLocalizer.CameraDirection.BACK, // cameraDirection
                    true, // useExtendedTracking
                    false, // enableCameraMonitoring
                    VuforiaLocalizer.Parameters.CameraMonitorFeedback.AXES, // cameraMonitorFeedback
                    0, // dx
                    0, // dy
                    0, // dz
                    0, // xAngle
                    -90, // yAngle
                    0, // zAngle
                    true); // useCompetitionFieldTargetLocations
            // Set min confidence threshold to 0.7
            tfodSkyStone.initialize(vuforiaSkyStone, 0.7F, true, true);
            // Initialize TFOD before waitForStart.
            // Init TFOD here so the object detection labels are visible
            // in the Camera Stream preview window on the Driver Station.
            tfodSkyStone.activate();
            telemetry.addData(">", "Press Play to start");
            telemetry.update();
            // Wait for start command from Driver Station.
            waitForStart();
            if (opModeIsActive()) {
                // Put run blocks here.
                while (opModeIsActive()) {
                    // Put loop blocks here.
                    // Get a list of recognitions from TFOD.
                    recognitions = tfodSkyStone.getRecognitions();
                    if (recognitions.size() > 0) {
                        left.setPower(1);
                        right.setPower(1);
                        sleep(700);
                        left.setPower(0);
                        right.setPower(0);
                    } else {
                        left.setPower(0);
                        right.setPower(0);
                    }
                    telemetry.update();
                }
            }
            // Deactivate TFOD.
            tfodSkyStone.deactivate();

            vuforiaSkyStone.close();
            tfodSkyStone.close();
        }

        /**
         * Display info (using telemetry) for a recognized object.
         */
        private void displayInfo(
                // TODO: Enter the type for argument named i
                UNKNOWN_TYPE i) {
            Recognition recognition;

            // Display label info.
            // Display the label and index number for the recognition.
            telemetry.addData("label " + i, recognition.getLabel());
            // Display upper corner info.
            // Display the location of the top left corner
            // of the detection boundary for the recognition
            telemetry.addData("Left, Top " + i, recognition.getLeft() + ", " + recognition.getTop());
            // Display lower corner info.
            // Display the location of the bottom right corner
            // of the detection boundary for the recognition
            telemetry.addData("Right, Bottom " + i, recognition.getRight() + ", " + recognition.getBottom());
        }
    }
